const colors = require('windicss/colors')
const typography = require('windicss/plugin/typography')

module.exports = {
  darkMode: 'class',
  plugins: [typography],
  theme: {
    fontFamily:{
      'sans': ['Poppins']
    },
    extend: {
      colors: {
        teal: colors.teal,
      },
    }
  },
}
