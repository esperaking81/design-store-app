import React from "react";

function Home() {
  const homeUtilities = [];

  for (let index = 0; index < 4; index++) {
    homeUtilities.push(<HomeUtility />);
  }

  return (
    <div className="dark:(bg-gray-900) py-2 space-y-10 flex flex-col font-sans">
      {/* Header */}
      <div className="flex items-center justify-between px-4">
        <div>
          <div>May 03, 2019</div>
          <div>Welcome home</div>
        </div>
        <div className="flex items-center p-2 border-2 border-gray-900 rounded-lg">
          <svg height="16" width="16" xmlns="http://www.w3.org/2000/svg">
            <path d="M8 0C3.588 0 0 3.588 0 8s3.588 8 8 8 8-3.588 8-8-3.588-8-8-8zm0 1c3.872 0 7 3.128 7 7s-3.128 7-7 7-7-3.128-7-7 3.128-7 7-7zm0 1C6.155 2 5 3.033 5 5c0 1.633.334 2.002 1 2.656v.563L3.22 10.06c-.142.097-.228.268-.22.438v1c0 .67 1 .64 1 0v-.75l2.78-1.844C6.914 8.816 7 8.66 7 8.5v-1c.003-.14-.055-.28-.156-.375C6.078 6.515 6.002 5.9 6 5c-.004-1.467.725-2.023 2-2 1.275.023 2 .587 2 2 0 1.34-.306 1.526-.813 2.094-.12.097-.192.25-.187.406v1c.002.16.087.316.22.406L12 10.75v.75c0 .662 1 .663 1 0v-1c.008-.17-.078-.342-.22-.438L10 8.22v-.595c.875-.72 1-1.195 1-2.625 0-2.005-1.155-3-3-3z" />
          </svg>
        </div>
      </div>

      {/* Home utilities */}
      <div className="grid grid-cols-2 gap-4 px-4">{homeUtilities}</div>

      {/* Accessories */}
      <div className="px-4">
        <p className="font-bold">Accessories</p>
        <div className="grid grid-cols-2 gap-4 mt-4">
          <div className="flex flex-col justify-between flex-grow p-4 bg-white rounded-lg">
            <div className="flex justify-between">
              <svg height="16" width="16" xmlns="http://www.w3.org/2000/svg">
                <path d="M2.156 2.862l.707.707c.482.48 1.16-.255.707-.708l-.707-.707c-.47-.47-1.18.235-.707.707zm9.28 0l.708-.707c.482-.482 1.16.255.707.707l-.706.707c-.47.47-1.18-.235-.707-.708zm-9.28 9.273l.707-.707c.482-.48 1.16.255.707.707l-.707.708c-.47.47-1.18-.235-.707-.708zM13.5 7h1c.68 0 .64 1 0 1h-1c-.666 0-.668-1 0-1zM.5 7h1c.68 0 .64 1 0 1h-1c-.666 0-.668-1 0-1zM8 13.5v1c0 .68-1 .64-1 0v-1c0-.666 1-.668 1 0zm0-13v1c0 .68-1 .64-1 0v-1c0-.666 1-.668 1 0zM7.5 3C5.02 3 3 5.02 3 7.5S5.02 12 7.5 12 12 9.98 12 7.5 9.98 3 7.5 3zm0 1C9.44 4 11 5.56 11 7.5S9.44 11 7.5 11 4 9.44 4 7.5 5.56 4 7.5 4zm3.937 8.133l.707.708c.482.482 1.16-.254.707-.707l-.706-.707c-.47-.47-1.18.235-.707.707z" />
              </svg>

              <div className="w-8 bg-blue-100 rounded-lg"></div>
            </div>

            <div className="mt-6">
              <div className="font-semibold">LAMP</div>
              <div className="text-gray-400">bedroom</div>
            </div>
          </div>
          <div className="p-4 bg-white rounded-lg">
            <div className="text-gray-400">Now Playing</div>
            <div className="font-semibold">Sound of Silence</div>
            <div className="font-semibold">- Simon</div>
            <div className="my-1 text-gray-400">living room</div>
            <AudioControllers />
          </div>
        </div>
        <div className="p-4 mt-4 bg-white rounded-lg">
          <div className="flex justify-between">
            <p className="font-semibold">Light inensity</p>
            <p className="text-blue-300">ON</p>
          </div>
          <div className="text-gray-400">living room</div>
          <div className="flex flex-row items-center mt-1 space-x-2">
            <div className="transform scale-80">
              <svg height="16" width="16" xmlns="http://www.w3.org/2000/svg">
                <path d="M2.156 2.862l.707.707c.482.48 1.16-.255.707-.708l-.707-.707c-.47-.47-1.18.235-.707.707zm9.28 0l.708-.707c.482-.482 1.16.255.707.707l-.706.707c-.47.47-1.18-.235-.707-.708zm-9.28 9.273l.707-.707c.482-.48 1.16.255.707.707l-.707.708c-.47.47-1.18-.235-.707-.708zM13.5 7h1c.68 0 .64 1 0 1h-1c-.666 0-.668-1 0-1zM.5 7h1c.68 0 .64 1 0 1h-1c-.666 0-.668-1 0-1zM8 13.5v1c0 .68-1 .64-1 0v-1c0-.666 1-.668 1 0zm0-13v1c0 .68-1 .64-1 0v-1c0-.666 1-.668 1 0zM7.5 3C5.02 3 3 5.02 3 7.5S5.02 12 7.5 12 12 9.98 12 7.5 9.98 3 7.5 3zm0 1C9.44 4 11 5.56 11 7.5S9.44 11 7.5 11 4 9.44 4 7.5 5.56 4 7.5 4zm3.937 8.133l.707.708c.482.482 1.16-.254.707-.707l-.706-.707c-.47-.47-1.18.235-.707.707z" />
              </svg>
            </div>
            <div className="relative flex items-center flex-grow h-6">
              <div className="w-full h-1 bg-gray-300 rounded-xl">
                <div className="w-1/2 h-full bg-blue-600 rounded-xl"></div>
              </div>
              <div className="absolute top-0 w-6 h-6 bg-blue-700 rounded-full right-1/2"></div>
            </div>
            <div>
              <svg height="16" width="16" xmlns="http://www.w3.org/2000/svg">
                <path d="M2.156 2.862l.707.707c.482.48 1.16-.255.707-.708l-.707-.707c-.47-.47-1.18.235-.707.707zm9.28 0l.708-.707c.482-.482 1.16.255.707.707l-.706.707c-.47.47-1.18-.235-.707-.708zm-9.28 9.273l.707-.707c.482-.48 1.16.255.707.707l-.707.708c-.47.47-1.18-.235-.707-.708zM13.5 7h1c.68 0 .64 1 0 1h-1c-.666 0-.668-1 0-1zM.5 7h1c.68 0 .64 1 0 1h-1c-.666 0-.668-1 0-1zM8 13.5v1c0 .68-1 .64-1 0v-1c0-.666 1-.668 1 0zm0-13v1c0 .68-1 .64-1 0v-1c0-.666 1-.668 1 0zM7.5 3C5.02 3 3 5.02 3 7.5S5.02 12 7.5 12 12 9.98 12 7.5 9.98 3 7.5 3zm0 1C9.44 4 11 5.56 11 7.5S9.44 11 7.5 11 4 9.44 4 7.5 5.56 4 7.5 4zm3.937 8.133l.707.708c.482.482 1.16-.254.707-.707l-.706-.707c-.47-.47-1.18.235-.707.707z" />
              </svg>
            </div>
          </div>
        </div>
      </div>

      <div className="flex-grow"></div>

      <div className="flex justify-between p-6 bg-white fixed left-0 right-0 bottom-0">
        <HomeIcon />
        <DashboardIcon />
        <UserIcon />
      </div>
    </div>
  );
}

function UserIcon() {
  return (
    <svg
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
      width="32"
      height="32"
    >
      <path d="M5 5a5 5 0 0 1 10 0v2A5 5 0 0 1 5 7V5zM0 16.68A19.9 19.9 0 0 1 10 14c3.64 0 7.06.97 10 2.68V20H0v-3.32z" />
    </svg>
  );
}

function DashboardIcon() {
  return (
    <svg
      height="32"
      width="32"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M10 18h5v-6h-5v6zm-6 0h5V5H4v13zm12 0h5v-6h-5v6zM10 5v6h11V5H10z" />
    </svg>
  );
}

function HomeIcon() {
  return (
    <svg
      className="feather feather-home"
      height="32"
      width="32"
      fill="none"
      stroke="#000"
      strokeLinecap="round"
      strokeWidth="1.5"
      viewBox="0 0 24 24"
      xmlns="http://www.w3.org/2000/svg"
    >
      <path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z" />
      <polyline points="9 22 9 12 15 12 15 22" />
    </svg>
  );
}

function AudioControllers() {
  return (
    <div className="flex justify-between">
      <svg
        height="24"
        width="24"
        viewBox="0 0 24 24"
        xmlns="http://www.w3.org/2000/svg"
        fill="gray"
      >
        <path d="M6 6h2v12H6zm3.5 6l8.5 6V6z" />
      </svg>
      <svg
        height="24"
        width="24"
        viewBox="0 0 24 24"
        fill="blue"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M8 5v14l11-7z" />
      </svg>
      <svg
        height="24"
        width="24"
        viewBox="0 0 24 24"
        xmlns="http://www.w3.org/2000/svg"
        fill="gray"
      >
        <path d="M6 18l8.5-6L6 6v12zM16 6v12h2V6h-2z" />
      </svg>
    </div>
  );
}

function HomeUtility() {
  return (
    <div className="p-4 bg-blue-400 rounded-lg">
      <svg
        height="16"
        width="16"
        stroke="#fff"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path d="M2.156 2.862l.707.707c.482.48 1.16-.255.707-.708l-.707-.707c-.47-.47-1.18.235-.707.707zm9.28 0l.708-.707c.482-.482 1.16.255.707.707l-.706.707c-.47.47-1.18-.235-.707-.708zm-9.28 9.273l.707-.707c.482-.48 1.16.255.707.707l-.707.708c-.47.47-1.18-.235-.707-.708zM13.5 7h1c.68 0 .64 1 0 1h-1c-.666 0-.668-1 0-1zM.5 7h1c.68 0 .64 1 0 1h-1c-.666 0-.668-1 0-1zM8 13.5v1c0 .68-1 .64-1 0v-1c0-.666 1-.668 1 0zm0-13v1c0 .68-1 .64-1 0v-1c0-.666 1-.668 1 0zM7.5 3C5.02 3 3 5.02 3 7.5S5.02 12 7.5 12 12 9.98 12 7.5 9.98 3 7.5 3zm0 1C9.44 4 11 5.56 11 7.5S9.44 11 7.5 11 4 9.44 4 7.5 5.56 4 7.5 4zm3.937 8.133l.707.708c.482.482 1.16-.254.707-.707l-.706-.707c-.47-.47-1.18.235-.707.707z" />
      </svg>
      <p className="mt-2">Morning</p>
    </div>
  );
}

export default Home;
