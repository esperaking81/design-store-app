import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./Home";
import Layout from "./Layout";

function App() {
  return (
    <div>
      <Router>
        <Layout>
          <Switch>
            <Route path="/about"></Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>
        </Layout>
      </Router>
    </div>
  );
}

export default App;
